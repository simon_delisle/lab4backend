const express = require('express');
const dotenv = require('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const connectDB = require('./config/db');
const cors = require('cors');


//load env vars
dotenv.config({ path: './config/config.env'});

connectDB();

//route files
const etudiant = require('./routes/etudiants');
const departement = require('./routes/departement');

const app = express();

//cors permet de faire le lien etre le back end et le https  doit etre en bad du express
app.use(cors());

//body parser
app.use(express.json());

//Mount routers
app.use('/etudiant', etudiant);
app.use('/departement', departement);



const PORT = process.env.PORT || 3477;


app.listen(
    PORT, console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`)
);